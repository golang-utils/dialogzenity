package dialogzenity

import (
	"gitlab.com/golang-utils/dialog"

	"github.com/ncruces/zenity"
)

/*
TODO

fix multiline texts before window content (e.g. select windows)
*/

func New(appname string, opts ...dialog.Option) dialog.App {
	opts = append(opts, dialog.WithGui(&App{}))
	return dialog.New(appname, opts...)
}

type App struct {
	dialog.AppExt
}

var _ dialog.GUI = &App{}

func init() {
	dialog.DefaultGUI = &App{}
}

func (a *App) SetApp(ap dialog.AppExt) {
	a.AppExt = ap
}

func (a *App) title() zenity.Option {
	var t = a.Name()
	var bc = a.GetBreadcrumb()

	if bc != "" {
		t = t + ": " + bc
	}
	return zenity.Title(t)
}

func (a *App) options(gopts dialog.GUIOptions) (opts []zenity.Option) {

	if gopts.Title != "" {
		opts = append(opts, zenity.Title(gopts.Title))
	} else {
		opts = append(opts, a.title())
	}

	if gopts.OkAsBack {
		opts = append(opts, zenity.OKLabel("back"))
	} else {
		opts = append(opts, zenity.OKLabel("OK"))
	}

	if a.HasBackNav() && !gopts.HideBack && !gopts.OkAsBack {
		opts = append(opts, zenity.ExtraButton("back"))
	}

	if gopts.Width > 0 {
		opts = append(opts, zenity.Width(gopts.Width))
	}

	if gopts.Height > 0 {
		opts = append(opts, zenity.Height(gopts.Height))
	}

	opts = append(opts, zenity.CancelLabel("quit"))

	return
}

func (a *App) Input(msg string, prefilled string, hideText bool, gopts dialog.GUIOptions) (res string, back bool, err error) {
	// Valid options: Title, Width, Height, OKLabel, CancelLabel, ExtraButton,
	// WindowIcon, Attach, Modal, EntryText, HideText.
	//
	// May return: ErrCanceled, ErrExtraButton.
	opts := a.options(gopts)

	if prefilled != "" {
		opts = append(opts, zenity.EntryText(prefilled))
	}

	if hideText {
		opts = append(opts, zenity.HideText())
	}

	res, err = zenity.Entry(msg, opts...)

	if err != nil {
		if err == zenity.ErrCanceled {
			//if err == zenity.ErrCanceled {
			err = dialog.ErrCanceled
		}

		if a.HasBackNav() && err == zenity.ErrExtraButton {
			return "", true, nil
		}

		return res, false, err
	}

	if gopts.OkAsBack {
		return res, true, nil
	}

	return res, false, err
}

func (a *App) Select(msg string, items []string, selected string, gopts dialog.GUIOptions) (res string, back bool, err error) {
	// Valid options: Title, Width, Height, OKLabel, CancelLabel, ExtraButton,
	// WindowIcon, Attach, Modal, RadioList, DefaultItems, DisallowEmpty.
	//
	// May return: ErrCanceled, ErrExtraButton, ErrUnsupported.
	opts := a.options(gopts)

	if selected != "" {
		opts = append(opts, zenity.DefaultItems(selected))
	}

	res, err = zenity.List(msg, items, opts...)

	if err != nil {
		if err == zenity.ErrCanceled {
			//if err == zenity.ErrCanceled {
			err = dialog.ErrCanceled
		}

		if a.HasBackNav() && err == zenity.ErrExtraButton {
			return "", true, nil
		}

		return res, false, err
	}

	if gopts.OkAsBack {
		return res, true, nil
	}

	return res, false, err
}

func (a *App) MultiSelect(msg string, items []string, preselected []string, gopts dialog.GUIOptions) (res []string, back bool, err error) {
	// Valid options: Title, Width, Height, OKLabel, CancelLabel, ExtraButton,
	// WindowIcon, Attach, Modal, CheckList, DefaultItems, DisallowEmpty.
	//
	// May return: ErrCanceled, ErrExtraButton, ErrUnsupported.

	opts := a.options(gopts)

	if len(preselected) > 0 {
		opts = append(opts, zenity.DefaultItems(preselected...))
	}

	res, err = zenity.ListMultiple(msg, items, opts...)

	if err != nil {
		if err == zenity.ErrCanceled {
			//if err == zenity.ErrCanceled {
			err = dialog.ErrCanceled
		}

		if a.HasBackNav() && err == zenity.ErrExtraButton {
			return nil, true, nil
		}

		return res, false, err
	}

	if gopts.OkAsBack {
		return res, true, nil
	}

	return res, false, err
}

func (a *App) Output(msg string, gopts dialog.GUIOptions) (back bool, err error) {
	// Valid options: Title, Width, Height, OKLabel, ExtraButton,
	// Icon, WindowIcon, Attach, Modal, NoWrap, Ellipsize.
	//
	// May return: ErrCanceled, ErrExtraButton.
	err = zenity.Info(msg, a.options(gopts)...)

	if err != nil {
		if err == zenity.ErrCanceled {
			//if err == zenity.ErrCanceled {
			err = dialog.ErrCanceled
		}

		if a.HasBackNav() && err == zenity.ErrExtraButton {
			return true, nil
		}

	}

	if gopts.OkAsBack {
		return true, nil
	}

	return false, err

	/*,
	//		zenity.Title("Information"),
	zenity.InfoIcon)
	*/
}

func (a *App) Error(msg string, gopts dialog.GUIOptions) {
	// Valid options: Title, Width, Height, OKLabel, ExtraButton,
	// Icon, WindowIcon, Attach, Modal, NoWrap, Ellipsize.
	//
	// May return: ErrCanceled, ErrExtraButton.
	opts := append([]zenity.Option{}, a.title(), zenity.OKLabel("back"), zenity.NoCancel())

	_ = zenity.Info(msg, opts...)

	return

	/*,
	//		zenity.Title("Information"),
	zenity.InfoIcon)
	*/
}
